int buzzer = 9; 

//---definimos las frecuencias de las notas
long DO  = 523.25;
long DoS = 554.37;
long RE  = 587.33;
long ReS = 622.25;
long MI  = 659.26;
long FA  = 698.46;
long FaS = 739.99;
long SOL = 783.99;
long SolS= 830.61;
long LA  = 880;
long LaS = 932.33;
long SI  = 987.77;
long Re2 = 1174.66;
long Fas2= 1479.98;

        
void setup(){
  pinMode(buzzer, OUTPUT);
}

void loop(){

 tone(buzzer, DO, 500);
 delay(1000);
 
 tone(buzzer, DoS, 500);
 delay(1000);
 
 tone(buzzer, RE, 500);
 delay(1000);
 
 tone(buzzer, ReS, 500);
 delay(1000);
 
 tone(buzzer, MI, 500);
 delay(1000);
   
 tone(buzzer, FA, 500);
 delay(1000);
 
 tone(buzzer, FaS, 500);
 delay(1000);

 tone(buzzer, SOL, 500);
 delay(1000);
 
 tone(buzzer, SolS, 500);
 delay(1000);
 
 tone(buzzer, LA, 500);
 delay(1000);
 
 tone(buzzer, Re2, 500);
 delay(1000);
 
 tone(buzzer, Fas2, 500);
 delay(1000);
}
