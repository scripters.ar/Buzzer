## Buzzer

En este repositorio encontraras información sobre como usar un buzzer, estará una lista con todas las notas musicales mas básicas para componer tu propio tema.

## Materiales

* **Una placa Arduino**
* **Un Buzzer**
* **Una resistencia de 100Ω**
* **Un Protoboard**

## Uso

```
En la carpeta diagrams encontraras la conexion del circuito.
```

```
En la carpeta src encontraras el codigo fuente de ejemplo.
```

En el codigo fuente encontraras las notas musicales mas basicas, para asi poder experimentar.

## Autor

* **Edermar Dominguez** - [Ederdoski](https://github.com/ederdoski)

## License

This code is open-sourced software licensed under the [MIT license.](https://opensource.org/licenses/MIT)

